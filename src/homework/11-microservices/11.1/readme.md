Выполнение [домашнего задания](https://github.com/netology-code/devkub-homeworks/blob/main/11-microservices-01-intro.md)
по теме "11.1. Введение в микросервисы"

## Q/A

### Задание 1

> Руководство крупного интернет магазина у которого постоянно растёт пользовательская база и количество заказов рассматривает возможность переделки своей внутренней ИТ системы на основе микросервисов.
> 
> Вас пригласили в качестве консультанта для оценки целесообразности перехода на микросервисную архитектуру.
> 
> Опишите какие выгоды может получить компания от перехода на микросервисную архитектуру и какие проблемы необходимо будет решить в первую очередь.

Самая главная проблема, которую предстоит решить - это разделение функционала интернет-магазина на домены.
На основе этих доменов будут создаваться микросервисы.

После решения данной проблемы можно получить следующие выгоды:
- изолированность доменов и сервисов. Это означает, что при отказе одного микросервиса или целого домена остальные будут работать.
  Например, при отказе сервиса оплат останется работать витрина и корзина, что положительно скажется на пользовательском опыте.
- упрощённое горизонтальное масштабирование. В отличие от монолита, микросервисы легче масштабировать при возникновении нагрузки.
  Достаточно только поднять количество реплик сервисов, которые испытывают нагрузку, в то время, как количество других сервисов останется без изменения. 
- построение структуры разработки на основе схемы доменов. Это означает, что для каждого домена (набора микросервисов)
  будет выделена своя команда разработки и поддержки. Таким образом, можно развивать функционал каждого домена максимально независимо друг от друга.
- вытекает из предыдущего: уменьшение значения TTM (time to market), то есть количество времени, прошедшее
  с момента взятия задачи в работу до её реального релиза на продуктовое окружение.  
