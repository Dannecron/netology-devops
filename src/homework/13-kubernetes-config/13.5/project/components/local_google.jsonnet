[
  {
    "apiVersion": "v1",
    "kind": "Endpoints",
    "metadata": {
      "name": "local-google",
    },
    "subsets": [
      {
        "addresses": [
          {
            "ip": "173.194.220.100"
          }
        ],
        "ports": [
          {
            "port": 80
          }
        ]
      }
    ]
  }
]
